# Final – Infraestructura en la nube


**Nombre y Apellido**

Nicolas Galván

**Materia**

Infraestructura en la Nube – Jueves 

(73041-2024-1C-Infraestructuras 22-División A-Día jueves)


# Introducción
En este documento se presentan instrucciones genéricas para la configuración de:

- Montado y formateo de un volumen de almacenamiento.
- Uso de Docker lanzando el servicio de apache2 asociando un volumen.
- Montar al volumen de apache2 un fichero html genérico.
- Configuración de nginx para el uso de proxy reverso.



# Requisitos
Los requisitos para las implementaciones son:

Sistema operativo Ubuntu

Acceso a internet

Docker instalado
# **Desarrollo**

# Volumen de almacenamiento

En este caso se va a montar un volumen de almacenamiento extra en una ubicación especifica. Previamente se debe agregar una unidad de almacenamiento al sistema.

Con el comando:
```
lsblk
```
se puede obtener una lista de los volúmenes actualmente disponibles para el sistema, pudiendo identificar al nuevo volumen de almacenamiento con el TYPE “disk”. También se puede utilizar el comando:
```
lsblk -f
```
para ver más detalles.

Primero de se debe dar formato a la unidad con el comando:
```
sudo mkfs.xfs /dev/sdb
```
(en este caso el volumen tiene como nombre “sdb”)

Luego se monta el volumen con formato en un directorio especifico con el comando 
```
sudo mount /dev/sdb /media
```
(en este caso se montó el volumen el directorio “/media”, pero este puede cambiar a elección).

## Apache 2 en Docker
Atención: Previamente a la ejecución de estos puntos, se debe contar con la correcta instalación de Docker.

Para la utilización del servicio Apache 2 en Docker se usará el siguiente comando.
```
sudo docker run -d --name="apache2" -v /media:/var/www/html -p 8181:80 ubuntu/apache2
```

Se usará el directorio “/media” (ubicación en donde se encuentra montado el volumen anteriormente creado) para colocar el fichero html a elección o el colocado en el repositorio <https://gitlab.com/nicolas.galvan/final-infraestructura-en-la-nube.git>.

Para la utilización del fichero html proporcionado se debe seguir la siguiente serie de comandos.
```
cd /media
```
```
sudo git clone https://gitlab.com/nicolas.galvan/final-infraestructura-en-la-nube.git
```
```
cd final-infraestructura-en-la-nube/
```
```
sudo cp index.html /media/
```

Para comprobar el funcionamiento de este, se puede acceder con la IP del sistema y el puerto indicado en el comando, en este caso el 8181, [**http://ip_sistema:8181**](http://ip_sistema:8181).

## Instalación y configuración proxy reverso
Para la utilización del servicio “Proxy reverso”, es necesario preferentemente otro sistema.

Una vez se tenga el entorno, se instala el servicio “nginx” con el comando:
```
sudo apt update && apt install nginx
```
Luego, dentro del archivo con ubicación “**/etc/nginx/sites-enabled/default**” se coloca la configuración:

```
server {
	listen 80 default_server;
	server_name _;

	location / {
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_pass http://44.211.46.14:8181/;
	}
}
```

Los datos deben modificarse según sea conveniente, como la IP y puerto del sistema con docker.

Para efectuar los cambios, es necesario reiniciar el servicio con el comando:
```
systemctl reload nginx
```

# Fuentes 

Las fuentes utilizadas fueron:

- Información de las clases
- w3schools.com
- <https://www.solvetic.com/tutoriales/article/3607-comandos-montar-desmontar-particiones-linux/>
